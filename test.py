import os
import keras
import librosa
import argparse
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from model.cnn import load_models
from keras.models import Sequential
from tensorflow.keras import layers, models
from audio.load_audio import load_audio_data
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession
from utils import max_pred, accuracy_caculate, rescalling_data

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)


def main():
    # testing settings
    parser = argparse.ArgumentParser(description='Baseline Mutiple Instance learning')
    parser.add_argument('--class_name', default=['fan', 'pump', 'valve', 'toycar', 'toycarconv'],
                        help='class')
    parser.add_argument('--data_test', default='./data/Audio_feature/check',
                        help='path to data test')
    parser.add_argument('--model_save', default='./checkpoint/CNN_ver4.h5',
                        help='save model CNN')

    # args, use cpu or gpu
    args = parser.parse_args()

    #load model and data
    CNN = load_models(args.model_save)
    Y_test, Y_label = load_audio_data(args.data_test, args.class_name)
    print(Y_test.shape)
    Y_test = rescalling_data(Y_test) 

    #prediction
    Y_Pred = max_pred(CNN.predict(Y_test), len(args.class_name))

    #caculate accuracy
    Acc = accuracy_caculate(Y_Pred, Y_label)
    print('Accuracy CNN = {}%'.format(Acc))


if __name__=="__main__":
    main()