import os
import sys
import librosa
import logging
import argparse
import numpy as np
import matplotlib.pyplot as plt
from audio.load_audio import load_audio, extract_mbe


def feat_extract(audio_folder, feat_folder, feat_test_folder, is_mono, sr, nfft, nb_mel_bands, class_name, ID):
    '''
    function: Extract feature of Audio data
    Input:  
        audio_folder    :Audio file direction 
        feat_folder     :Audio feature file direction 
        is_mono         :True if Audio have one channel
        sr              :Sample rate
        nfft           
        nb_mel_bands    :Number of mel filter
    '''
    audio_folder = audio_folder + class_name + '/'
    num = 0
    for audio_filename in os.listdir(audio_folder):
        file_ID = audio_filename.split('_')
        if file_ID[2] == ID:
            audio_file = os.path.join(audio_folder, audio_filename)
            y, _ = load_audio(audio_file, mono=is_mono, fs=sr)
            mbe = None

            #if Audio is an
            if is_mono:
                mbe = extract_mbe(y, sr, nfft, nb_mel_bands).T
            else:
                for ch in range(y.shape[0]):
                    mbe_ch = extract_mbe(y[ch, :], sr, nfft, nb_mel_bands).T
                    if mbe is None:
                        mbe = mbe_ch
                    else:
                        mbe = np.concatenate((mbe, mbe_ch), 1)
            if num < 50:
                tmp_feat_file_test = os.path.join(feat_test_folder, '{}_{}_{}_{}_{}.npz'.format(class_name, file_ID[0], file_ID[2], file_ID[3], 'mon' if is_mono else 'bin'))
                np.savez(tmp_feat_file_test, mbe)
            else:
                tmp_feat_file = os.path.join(feat_folder, '{}_{}_{}_{}_{}.npz'.format(class_name, file_ID[0], file_ID[2], file_ID[3], 'mon' if is_mono else 'bin'))
                np.savez(tmp_feat_file, mbe)
            num = num + 1       


def main():
    # arguments settings
    parser = argparse.ArgumentParser(description='Baseline Mutiple Instance learning')
    parser.add_argument('--is_mono', default=True,
                        help='Audio is single channel or not')
    parser.add_argument('--sr', type=int, default=16000, metavar='N',
                        help='sample rate')
    parser.add_argument('--nfft', type=int, default=2048, metavar='N',
                        help='number of fast furier transform')
    parser.add_argument('--nb_mel_bands', type=int, default=40, metavar='N',
                        help='number of mel filter banks')
    parser.add_argument('--class_name', default=['fan', 'pump', 'valve', 'toycar', 'toycarconv'],
                        help='class')
    parser.add_argument('--audio_folder', default='./data/Audio/Train/',
                        help='folder audio')
    parser.add_argument('--audio_check_folder', default='./data/Audio/check/',
                        help='data for check accuracy')
    parser.add_argument('--feat_folder', default='./data/Audio_feature/Train',
                        help='data after processing for train')
    parser.add_argument('--feat_test_folder', default='./data/Audio_feature/Test',
                        help='data after processing for test')
    parser.add_argument('--feat_check_folder', default='./data/Audio_feature/check',
                        help='data after processing for check accuracy')

    # args, use cpu or gpu
    args = parser.parse_args()
    
    #Convert mp4 to wav
    #conv_mp4_to_wav(video_folder, audio_folder)
    
    for i in range (0, len(args.class_name), 1):
        if (args.class_name[i] != 'toycar') and (args.class_name[i] != 'toycarconv'):
            ID = '00'
        else:
            ID = '01'
        #extract audio feature
        feat_extract(args.audio_folder, args.feat_folder, args.feat_test_folder, args.is_mono, 
                    args.sr, args.nfft, args.nb_mel_bands, args.class_name[i], ID)
    
        #Extract audio checking feature
        feat_extract(args.audio_check_folder, args.feat_check_folder, args.feat_check_folder,  args.is_mono, 
                    args.sr, args.nfft, args.nb_mel_bands, args.class_name[i], ID)

if __name__=="__main__":
    main()