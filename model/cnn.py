import keras
import tensorflow as tf
from keras.models import Sequential
from tensorflow.keras import layers, models


def CNN_model(data_in, num_class):
    '''
    Function: Create CNN Model
    Input:
        input_data: Data input model
        num_class: Number of class in Data
    Ouput:
        model CNN
    '''
    #Model start
    model = models.Sequential()
    model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=data_in.shape))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Dropout(.25))
    model.add(layers.Flatten())
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dropout(.5))
    model.add(layers.Dense(num_class, activation='softmax'))

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999),
                loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                metrics=['accuracy'])
    
    model.summary()
    return model


def load_models(models_dic):
    model = tf.keras.models.load_model(models_dic)
    return model