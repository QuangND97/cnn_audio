import os
import keras
import librosa
import logging
import argparse
import datetime
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from model.cnn import CNN_model
from utils import rescalling_data
from keras.models import Sequential
from audio.load_audio import data_generator
from tensorflow.keras import layers, models
from audio.load_audio import load_audio_data
from tensorflow.compat.v1 import ConfigProto, InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)


def main():
    # training settings
    parser = argparse.ArgumentParser(description='Baseline Mutiple Instance learning')
    parser.add_argument('--epochs', type=int, default=50, metavar='N',
                        help='number epochs')
    parser.add_argument('--lr', type=float, default=0.001, metavar='LR',
                        help='learning rate')
    parser.add_argument('--batch_size', type=int, default=32, metavar='N',
                        help='batch_size')
    parser.add_argument('--input_size', default=np.zeros((40, 157, 1), dtype=int), type=int,
                        help='size of input')
    parser.add_argument('--class_name', default=['fan', 'pump', 'valve', 'toycar', 'toycarconv'],
                        help='class')
    parser.add_argument('--data_train', default='./data/Audio_feature/Train',
                        help='path to data train')
    parser.add_argument('--data_test', default='./data/Audio_feature/Test',
                        help='path to data test')
    parser.add_argument('--model_save', default='./checkpoint/CNN_ver4.h5',
                        help='save model CNN ver 1')

    # args, use cpu or gpu
    args = parser.parse_args()

    list_filename = []
    for audio_filename in os.listdir(args.data_train):
        list_filename = np.append(list_filename, audio_filename)
    number_batch = int(len(list_filename)/args.batch_size)
    x_val, x_val_label = load_audio_data(args.data_test, args.class_name)
    
  
    CNN =  CNN_model(args.input_size, len(args.class_name))

    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
    CNN.fit(x=data_generator(args.data_train, args.batch_size, list_filename, args.class_name), 
            steps_per_epoch=number_batch, epochs=args.epochs,
            validation_data=(x_val, x_val_label),
            callbacks=[tensorboard_callback])

    CNN.save(args.model_save)


if __name__=="__main__":
    #Place tensors on the CPU
    with tf.device('/GPU:0'):
        main()