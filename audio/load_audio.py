import os
import sys
import wave
import librosa
import numpy as np
from utils import rescalling_data

def load_audio(filename, mono=True, fs=44100):
    '''
    Function: Read audio data from audio file 
    Input: 
        filename: Audio file
        mono:     True if Audio channel ís 1 else False
        fs:       x2 frequency max of the audio
    Output:
        audio_data:
        sample_rate: sampling frequency
    '''
    file_base, file_extension = os.path.splitext(filename)
    print(file_base)
    if file_extension == '.wav':
        _audio_file = wave.open(filename)

        # Audio info
        sample_rate = _audio_file.getframerate()
        sample_width = _audio_file.getsampwidth()
        number_of_channels = _audio_file.getnchannels()
        number_of_frames = _audio_file.getnframes()

        # Read raw bytes
        data = _audio_file.readframes(number_of_frames)
        _audio_file.close()

        # Convert bytes based on sample_width
        num_samples, remainder = divmod(len(data), sample_width * number_of_channels)
        if remainder > 0:
            raise ValueError('The length of data is not a multiple of sample size * number of channels.')
        if sample_width > 4:
            raise ValueError('Sample size cannot be bigger than 4 bytes.')

        if sample_width == 3:
            # 24 bit audio
            a = np.empty((num_samples, number_of_channels, 4), dtype=np.uint8)
            raw_bytes = np.frombuffer(data, dtype=np.uint8)
            a[:, :, :sample_width] = raw_bytes.reshape(-1, number_of_channels, sample_width)
            a[:, :, sample_width:] = (a[:, :, sample_width - 1:sample_width] >> 7) * 255
            audio_data = a.view('<i4').reshape(a.shape[:-1]).T
        else:
            # 8 bit samples are stored as unsigned ints; others as signed ints.
            dt_char = 'u' if sample_width == 1 else 'i'
            a = np.frombuffer(data, dtype='<%s%d' % (dt_char, sample_width))
            audio_data = a.reshape(-1, number_of_channels).T

        if mono:
            # Down-mix audio
            audio_data = np.mean(audio_data, axis=0)

        # Convert int values into float
        audio_data = audio_data / float(2 ** (sample_width * 8 - 1) + 1)

        # Resample
        if fs != sample_rate:
            audio_data = librosa.core.resample(audio_data, sample_rate, fs)
            sample_rate = fs

        return audio_data, sample_rate
    return None, None


def extract_mbe(_y, _sr, _nfft, _nb_mel):
    '''
    function: Caculate mel band energy from audio data
    Input:
        _y:     Audio data
        _sr: s  ample rate
        _nfft:  number of fast transform
        _nb_mel:number of mel filter
    Output:
        np.log(np.dot(mel_basis, spec)): Logarit of Mel band energy
    '''
    # Calculate Power Spectrum: spec = |STFT(y)|
    spec, n_fft = librosa.core.spectrum._spectrogram(y=_y, n_fft=_nfft, hop_length=_nfft//2, power=1)
    
    # Calculate Filter Banks (FBs)
    if n_fft == _nfft:
        mel_basis = librosa.filters.mel(sr=_sr, n_fft=_nfft, n_mels=_nb_mel)
    # Apply FBs to the Power Spectrum
    return np.log(np.dot(mel_basis, spec))


def load_audio_data(audio_folder, class_):
    '''
    Function: Load preprocessing audio data
    Input:
        audio_folder: folder that have audio file data after processing
    Ouput:
        _X_train: Data for trainning
        _X_test: Data for testing
    '''
    X_data = np.zeros((1, 40, 157, 1))
    X_label = 0
    for audio_filename in os.listdir(audio_folder):
        file_ID = audio_filename.split('_')
        for i in range (0, len(class_), 1):
            if file_ID[0] == class_[i]:
                audio_file = os.path.join(audio_folder, audio_filename)
                dmp = np.load(audio_file)
                X = dmp['arr_0']
                X = X[:157,:]
                X = X.reshape((1, X.shape[1], X.shape[0], 1))
                X_data = np.concatenate((X_data, X), axis=0)
                X_label = np.append(X_label, i)
    X_data = rescalling_data(X_data)
    return X_data[1:,:,:,:], X_label[1:]


def data_generator(audio_folder, batch_size, list_filename, class_):
    """
    Generate data for training
    """
    number = 0
    while True:
        X_data = np.zeros((1, 40, 157, 1))
        X_label = 0
        for idx in range(batch_size*number, batch_size*(number + 1), 1):
            file_ID = list_filename[idx].split('_')
            for i in range (0, len(class_), 1):
                if file_ID[0] == class_[i]:
                    audio_file = os.path.join(audio_folder, list_filename[idx])
                    dmp = np.load(audio_file)
                    X = dmp['arr_0']
                    X = X[:157,:]
                    X = X.reshape((1, X.shape[1], X.shape[0], 1))
                    X_data = np.concatenate((X_data, X), axis=0)
                    X_label = np.append(X_label, i)
        X_data = rescalling_data(X_data)
        number = number + 1
        if number == int(len(list_filename)/batch_size):
            number = 0
        yield X_data[1:,:,:,:], X_label[1:]
    