"""The utils.py script has some utility functions."""
import os
import logging
import numpy as np
import tensorflow as tf
from moviepy.editor import VideoFileClip


def Reshape_2D_4D(y, shape1, shape2):
    x_total = np.zeros((1, shape1, shape2, 1))
    shape0 = int(y.shape[0]/shape1)
    for i in range(0, shape0, 1):
        x_size =  y[i*shape1:(i+1)*shape1, :]
        x_size = x_size.reshape((1, x_size.shape[0], x_size.shape[1], 1))
        x_total = np.concatenate((x_total, x_size), axis=0)
    return x_total[1:,:,:,:]


def max_pred(data_in, num_class):
    x_pred = 0
    for i in range(0, len(data_in), 1):
        max_i = np.amax(data_in[i])
        for j in range(0, num_class, 1):
            if data_in[i][j] == max_i:
                x_pred = np.append(x_pred, j)
    return x_pred[1:]


def accuracy_caculate(pred, label):
    TP = 0
    for i in range (0, len(pred), 1):
        if pred[i] == label[i]:
            TP = TP + 1
    return (TP/len(pred)*100)


def rescalling_data(Y_test):
    # Normalize features using rescalling
    Y_max = np.amax(Y_test)
    Y_min = np.amin(Y_test)

    Y_test = (Y_test - Y_min)/(Y_max - Y_min)
    return Y_test


def conv_mp4_to_wav(video_folder, audio_folder):
    '''
    Function: Convert MP4 file to WAV file
    Input:
        video_folder
        audio_folder
    '''
    for video_filename in os.listdir(video_folder):
        file_properties = video_filename.split('.')
        video_file = os.path.join(video_folder, video_filename)
        video = VideoFileClip(video_file)
        audio = video.audio
        audio.write_audiofile(audio_folder + file_properties[0] + '.wav')
